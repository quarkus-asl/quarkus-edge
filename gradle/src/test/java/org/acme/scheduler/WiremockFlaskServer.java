package org.acme.scheduler;

import java.util.Collections;
import java.util.Map;

import com.github.tomakehurst.wiremock.WireMockServer;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import io.quarkus.test.junit.callback.QuarkusTestAfterEachCallback;
import io.quarkus.test.junit.callback.QuarkusTestMethodContext;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class WiremockFlaskServer implements QuarkusTestResourceLifecycleManager, QuarkusTestAfterEachCallback {

	private WireMockServer wireMockServer;
	

	@Override
	public Map<String, String> start() {
		wireMockServer = new WireMockServer();
		wireMockServer.start(); 

		stubFor(get(urlEqualTo("/v1/gas"))   
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody(
								"[{" +
										"\"adc\": \"1\"," +
										"\"nh3\": \"2\"" +
										"}]"
								)));

		stubFor(get(urlMatching(".*")).atPriority(10).willReturn(aResponse().proxiedFrom("http://localhost")));   

		return Collections.singletonMap("org.acme.scheduler.CountResource/mp-rest/url", wireMockServer.baseUrl()); 
	}

	@Override
	public void stop() {
		if (null != wireMockServer) {
			wireMockServer.stop();
		}
	}

	@Override
	public void afterEach(QuarkusTestMethodContext context) {
		// TODO Auto-generated method stub
		
	}

}
