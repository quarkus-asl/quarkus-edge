package org.acme.scheduler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/v1")
@RegisterRestClient(configKey = "datacollection-api")
public interface RestDataCollectionClientService {
	
	@GET
	@Path("/gas")
	@Produces(MediaType.APPLICATION_JSON)
	String getGas();
	
	@GET
	@Path("/pollution")
	@Produces(MediaType.APPLICATION_JSON)
	String getPollution();

}
