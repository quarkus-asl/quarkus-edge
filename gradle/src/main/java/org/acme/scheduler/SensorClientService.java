package org.acme.scheduler;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/v1")
@RegisterRestClient(configKey="datacollection-api")
public interface SensorClientService {
	
    @GET
    @Path("/gas")
    @Produces(MediaType.TEXT_PLAIN)
    String getGas() throws Exception;

    @GET
    @Path("/pollution")
    @Produces(MediaType.TEXT_PLAIN)
    String getPollution() throws Exception;
    
    @GET
    @Path("/serial")
    @Produces(MediaType.TEXT_PLAIN)
    String getSerial() throws Exception;

}
