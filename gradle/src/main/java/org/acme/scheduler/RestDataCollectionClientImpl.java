package org.acme.scheduler;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RestClient;

public class RestDataCollectionClientImpl {
	
	@Inject
	@RestClient
	RestDataCollectionClientService dataService;
	
	@GET
	@Path("/gas")
	@Produces(MediaType.APPLICATION_JSON)
	public String getGas() {
		return dataService.getGas();
	}
	
	@GET
	@Path("/pollution")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPollution() {
		return dataService.getPollution();
	}

}
