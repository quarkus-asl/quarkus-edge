package org.acme.scheduler;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class CountResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/count")
          .then()
             .statusCode(200)
             .body(containsString("coord"));
    }
    
    @Test
    public void testGasEndpoint() {
        given()
          .when().get("/gas")
          .then()
             .statusCode(200)
             .body("$.size()", is(1),
            	   "[0].adc", is(1),
            	   "[0].nh3", is(2)
             );
    }
    
    @Test
    public void testPollutionEndpoint() {
        given()
          .when().get("/pollution")
          .then()
             .statusCode(200)
             .body("$.size()", is(1),
              	   "[0].PM1_0", is(1),
              	   "[0].PM2_5", is(2)
             );
    }

}