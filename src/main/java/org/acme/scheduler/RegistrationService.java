package org.acme.scheduler;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("/v1/register")
@RegisterRestClient(configKey="registration-api")
public interface RegistrationService {
	
    @DELETE
    @Path("/id/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    void deregister(@PathParam int id);
    
    @PUT
    @Path("/serial/{serial}/name/{name}/longitude/{longitude}/latitude/{latitude}")
    @Produces(MediaType.TEXT_PLAIN)
    String register(@PathParam String serial, @PathParam String name, @PathParam double longitude, @PathParam double latitude);

}
