package org.acme.scheduler;

public class CoordinatesBean {
	
	
	private double longitude = 0;
	private double latitude = 0;
	
	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	
    @Override
    public String toString() {
	return "CoordinatesBean [lat =" + latitude + ", long =" + longitude + "]";
    }
	
}
