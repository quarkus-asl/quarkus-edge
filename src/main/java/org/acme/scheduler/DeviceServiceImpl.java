package org.acme.scheduler;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import io.quarkus.scheduler.Scheduled;

@ApplicationScoped
public class DeviceServiceImpl {
	
    @ConfigProperty(name = "registration.address.default")
    String address;
    @ConfigProperty(name ="registation.name.default")
    String deviceName;
    
    String deviceId;

	@Inject
	Logger LOGGER;
	
	@Inject
	CoordinatesServiceImpl coordsService;
	
	@Inject
	@RestClient
	SensorClientService sensorService;
	
	@Inject
	@RestClient
	RegistrationService registrationService;
	
	public void onStart(@Observes StartupEvent ev) throws Exception {
		LOGGER.info("The application is starting...{}");
	}
	
    void onStop(@Observes ShutdownEvent ev) {       
    	LOGGER.info("The application is stopping...");
    	registrationService.deregister(Integer.parseInt(deviceId));
    }
	
	@PostConstruct
	void postConstruct() throws Exception {
		// The registration needs to go here not in onStart 
		// due to timing with run
		register();
	}
     
    private void register() throws Exception {
    	// Get the serial id from Flask
    	// Get the coordinates of the Pi
    	// Send the registration message
    	
    	//String serialNumber = sensorService.getSerial();
    	String serialNumber = "000000001d37578f";
    	CoordinatesBean coordinatesBean = null;
    	
    	coordinatesBean = coordsService.getCoordinates(address);
    	LOGGER.info(coordinatesBean.toString());
    	
    	deviceId = registrationService.register(serialNumber, deviceName, coordinatesBean.getLongitude(), coordinatesBean.getLatitude());
	
    	LOGGER.info("Registered with device ID: " + deviceId);
    }

    @Scheduled(every = "5s")
	void run() {
    	// Read gas
    	// Decorate gas
    	// send gas to hub
    	// Read pollution
    	// Decorate pollution
    	// send pollution to hub
    	LOGGER.info("Running...");
    }
}
